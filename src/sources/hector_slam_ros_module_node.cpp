//////////////////////////////////////////////////////
//
//  Created on: Sept 10, 2016
//      Author: Hriday Bavle
//
//////////////////////////////////////////////////////



//I/O Stream
//std::cout
#include <iostream>

#include <string>


// ROS
#include "ros/ros.h"

//ROSModule
#include "hector_slam_ros_module.h"

using namespace std;

int main(int argc, char **argv)
{
    //Init
    ros::init(argc, argv, "hectorSlamROSModule");        //Say to ROS the name of the node and the parameters
    ros::NodeHandle n;                                  //Este nodo admite argumentos!!

    std::string node_name=ros::this_node::getName();
    cout<<"node name="<<node_name<<endl;


    //Class definition
    hectorSlamROSModule MyhectorSlamROSModule;

    //Open!
    MyhectorSlamROSModule.open(n);


    //Loop -> Ashyncronous Module
    while(ros::ok())
    {
        ros::spin();
    }

    return 1;
}

