//////////////////////////////////////////////////////
//  droneExamplePackageROSModuleSource.cpp
//
//  Created on: Sep 10, 2016
//      Author: Hriday Bavle
//
//////////////////////////////////////////////////////


#include "hector_slam_ros_module.h"

// using namespaces only in cpp files!!
using namespace std;


hectorSlamROSModule::hectorSlamROSModule() : DroneModule(droneModule::active)
{

    return;
}

hectorSlamROSModule::~hectorSlamROSModule()
{
	close();
	return;
}

bool hectorSlamROSModule::init()
{
    DroneModule::init();
    counter1 = 0;
    counter2 = 0;
    return true;
}


void hectorSlamROSModule::open(ros::NodeHandle & nIn)
{
	//Node
    DroneModule::open(nIn);


    //Init
    if(!init())
    {
        cout<<"Error init"<<endl;
        return;
    }


    //// TOPICS
    //Subscribers
    //droneLaserScanSimSub    = n.subscribe("scan", 1, &hectorSlamROSModule::dronelaserScanSimCallback, this);
    droneLaserScanSub       = n.subscribe("scan",1, &hectorSlamROSModule::droneLaserScanCallback, this);
    dronePoseSub            = n.subscribe("odometry/filtered",1, &hectorSlamROSModule::dronePoseCallback, this);
   // droneHectorSlamPoseSub  = n.subscribe("poseupdate",1, &hectorSlamROSModule::droneHectorSlamPoseCallback, this);

    //Publishers
    droneLaserScanPub           = n.advertise<sensor_msgs::LaserScan>("scan_data",1, this);
//    droneHectorSlamPosePub      = n.advertise<geometry_msgs::PoseWithCovarianceStamped>("hector_slam_pose_out", 1, true);
    droneHectorSlamInitPosePub  = n.advertise<geometry_msgs::PoseWithCovarianceStamped>("initialpose",1,true);


    //Flag of module opened
    droneModuleOpened=true;


    //autostart module!
//    moduleStarted=true;

	
	//End
	return;
}


void hectorSlamROSModule::close()
{
    DroneModule::close();


    //Do stuff

    return;
}


bool hectorSlamROSModule::resetValues()
{
    //Do stuff
    counter1 = 0;
    counter2 = 0;
    return true;
}


bool hectorSlamROSModule::startVal()
{
    //Do stuff
    counter1 = 0;
    counter2 = 0;
    //End
    return DroneModule::startVal();
}


bool hectorSlamROSModule::stopVal()
{
    //Do stuff

    counter1 = 0;
    counter2 = 0;
    return DroneModule::stopVal();
}


bool hectorSlamROSModule::run()
{
    if(!DroneModule::run())
        return false;

    if(droneModuleOpened==false)
        return false;

    return true;
}


void hectorSlamROSModule::droneLaserScanCallback(const sensor_msgs::LaserScan &msg)
{
    if(moduleStarted == true){
        laser_scan_data = msg;
        publishLaserScan();
    }
}

//void hectorSlamROSModule::dronelaserScanSimCallback(const sensor_msgs::LaserScan &msg)
//{
//    if(moduleStarted == true){
//        laser_scan_data = msg;
//        publishLaserScan();
//    }
//}

void hectorSlamROSModule::dronePoseCallback(const nav_msgs::Odometry& msg)
{
    if(moduleStarted == true){
        if(counter1 == 0){
            init_pose_data = msg;
            counter1++;
        }
        publishHectorSlamInitPose();
    }
}


//void hectorSlamROSModule::droneHectorSlamPoseCallback(const geometry_msgs::PoseWithCovarianceStamped &msg)
//{
//    if(moduleStarted == true){

//        hector_slam_pose = msg;

//        hector_slam_pose.pose.pose.position.x = hector_slam_pose.pose.pose.position.x + init_pose_data.x;
//        hector_slam_pose.pose.pose.position.y = hector_slam_pose.pose.pose.position.y + init_pose_data.y;
//        publishHectorSlamPose();
//    }
//}

//bool hectorSlamROSModule::publishHectorSlamPose()
//{
//    if(moduleStarted == true){
//        droneHectorSlamPosePub.publish(hector_slam_pose);
//    }

//    return true;
//}


bool hectorSlamROSModule::publishLaserScan()
{
    if(moduleStarted == true){
        droneLaserScanPub.publish(laser_scan_data);
    }

    return true;
}

bool hectorSlamROSModule::publishHectorSlamInitPose()
{
    if(moduleStarted == true)
    {
        if(counter2 == 0)
        {
            hector_slam_init_pose.header.frame_id       = "map";
            hector_slam_init_pose.pose.pose.position    = init_pose_data.pose.pose.position;
            hector_slam_init_pose.pose.pose.orientation = init_pose_data.pose.pose.orientation;

            droneHectorSlamInitPosePub.publish(hector_slam_init_pose);
            counter2++;
        }

    }
}








