//////////////////////////////////////////////////////
//  DroneExamplePackageROSModule.h
//
//  Created on: Sep 10, 2016
//      Author: Hriday Bavle
//
//////////////////////////////////////////////////////

#ifndef DRONE_EXAMPLE_PACKAGE_ROS_MODULE_H
#define DRONE_EXAMPLE_PACKAGE_ROS_MODULE_H

//I/O stream
//std::cout
#include <iostream>

//Vector
//std::vector
#include <vector>

//String
//std::string, std::getline()
#include <string>

//String stream
//std::istringstream
#include <sstream>

//File Stream
//std::ofstream, std::ifstream
#include <fstream>

//String stream
//std::istringstream
#include <sstream>


// ROS
#include "ros/ros.h"

//Drone module
#include "droneModuleROS.h"

//ROS Messages
#include "sensor_msgs/LaserScan.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "nav_msgs/Odometry.h"

//droneMsgROS
#include "droneMsgsROS/dronePose.h"

/////////////////////////////////////////
//
//   Description
//
/////////////////////////////////////////
class hectorSlamROSModule : public DroneModule
{	

    //subscribers
protected:
//     ros::Subscriber droneLaserScanSimSub;
     ros::Subscriber droneLaserScanSub;
     ros::Subscriber dronePoseSub;
     ros::Subscriber droneHectorSlamPoseSub;

public:
    //Callback function
//    void dronelaserScanSimCallback(const sensor_msgs::LaserScan& msg);
    void droneLaserScanCallback(const sensor_msgs::LaserScan& msg);
    void dronePoseCallback(const nav_msgs::Odometry& msg);
    void droneHectorSlamPoseCallback(const geometry_msgs::PoseWithCovarianceStamped& msg);

    //publishers
protected:
    ros::Publisher droneLaserScanPub;
    ros::Publisher droneHectorSlamPosePub;
    ros::Publisher droneHectorSlamInitPosePub;

    //Function
    bool publishLaserScan();
    bool publishHectorSlamPose();
    bool publishHectorSlamInitPose();


protected:
    sensor_msgs::LaserScan laser_scan_data;
    nav_msgs::Odometry init_pose_data;
    geometry_msgs::PoseWithCovarianceStamped hector_slam_pose, hector_slam_init_pose;
    int counter1, counter2;

public:
    hectorSlamROSModule();
    ~hectorSlamROSModule();
	
public:
    void open(ros::NodeHandle & nIn);
	void close();

protected:
    bool init();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};

#endif
