cmake_minimum_required(VERSION 2.4.6)

set(PROJECT_NAME hector_slam_ros_module)
project(${PROJECT_NAME})

#include($ENV{ROS_ROOT}/core/rosbuild/rosbuild.cmake)


### Use version 2011 of C++ (c++11). By default ROS uses c++98
#see: http://stackoverflow.com/questions/10851247/how-to-activate-c-11-in-cmake
#see: http://stackoverflow.com/questions/10984442/how-to-detect-c11-support-of-a-compiler-with-cmake
#add_definitions(-std=c++11)
#add_definitions(-std=c++0x)
add_definitions(-std=c++03)


set(HECTOR_SLAM_ROS_MODULE_SOURCE_DIR
	src/sources)
	
set(HECTOR_SLAM_ROS_MODULE_INCLUDE_DIR
	src/include
	)

set(HECTOR_SLAM_ROS_MODULE_SOURCE_FILES

	#General
	${HECTOR_SLAM_ROS_MODULE_SOURCE_DIR}/hector_slam_ros_module.cpp

	
	)
	
set(HECTOR_SLAM_ROS_MODULE_HEADER_FILES

	#General
	${HECTOR_SLAM_ROS_MODULE_INCLUDE_DIR}/hector_slam_ros_module.h

	
	
	)
	

#Nodes
set(HECTOR_SLAM_ROS_MODULE_NODE_SOURCE_FILES
    src/sources/hector_slam_ros_module_node.cpp
)
	


find_package(catkin REQUIRED
		COMPONENTS roscpp droneModuleROS)
		

catkin_package(
        CATKIN_DEPENDS roscpp droneModuleROS)


include_directories(${catkin_INCLUDE_DIRS})



#NODES
include_directories(${HECTOR_SLAM_ROS_MODULE_INCLUDE_DIR})
add_executable(hectorSlamROSModuleNode ${HECTOR_SLAM_ROS_MODULE_NODE_SOURCE_FILES} ${HECTOR_SLAM_ROS_MODULE_SOURCE_FILES} ${HECTOR_SLAM_ROS_MODULE_HEADER_FILES})


target_link_libraries(hectorSlamROSModuleNode ${catkin_LIBRARIES})

set(OTHER_FILES
        package.xml
)
add_custom_target(other_files SOURCES ${OTHER_FILES})


